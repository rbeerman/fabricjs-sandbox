var $ = function(id){return document.getElementById(id)};

var isFilling = false;
var brushWidth = 5;
var actionStack = [];
var lastAction = {};
var drawingLineWidthEl = $('drawing-line-width');
var canvas = new fabric.Canvas('c');

fabric.Image.filters.TransparentWhite = fabric.util.createClass(fabric.Image.filters.BaseFilter, {
    type: 'TransparentWhite',



    applyTo2d: function(options) {
        var imageData = options.imageData,
            data = imageData.data, i, len = data.length;

        for (i = 0; i < len; i += 4) {
            if (data[i] == 0 && data[i + 1] == 0 && data[i + 2] == 0) {
                data[i + 3] = 0.0;
            }
            
        }
    }            
});

fabric.Image.filters.TransparentWhite.fromObject = fabric.Image.filters.BaseFilter.fromObject;

var imgElement = document.getElementById('my-image');
var imgInstance = new fabric.Image(imgElement, {
    left: 0,
    top: 0,
    opacity: 1.00
});
imgInstance.scaleToWidth(704, true);
imgInstance.scaleToHeight(720, true);
imgInstance.selectable = false;

imgInstance.filters.push(new fabric.Image.filters.TransparentWhite());
imgInstance.applyFilters();

canvas.add(imgInstance);

/* fabric.Image.fromURL('female.png', function(img) {
    img.filters.push(new fabric.Image.filters.TransparentWhite());
    img.applyFilters();
    canvas.add(img);
}); */



//let outlineImage = new Image();

//outlineImage.src = imgInstance.toDataURL();
/*canvas.setBackgroundImage(imgInstance, function() {
    console.log("Background image loaded");
    
});*/


canvas.freeDrawingBrush.width = brushWidth;
canvas.freeDrawingBrush.color = 'rgb(191, 96, 0)';
canvas.isDrawingMode = true;
canvas.preserveObjectStacking = true;
canvas.selection = false;
canvas.hoverCursor = 'point';
//o.perPixelTargetFind = true;

canvas.on('mouse:up', function() {
    if (canvas.isDrawingMode) {
        lastAction = {'action': 'draw', 'target': canvas.item(canvas.getObjects().length - 1)};
        actionStack.push(lastAction);
    }
});

drawingLineWidthEl.value = brushWidth;

drawingLineWidthEl.onchange = function() {
    canvas.freeDrawingBrush.width = parseInt(this.value, 10) || 1;
    document.getElementById('width-label').innerHTML = this.value;
};

function printSerialization() {
    var canvasObj = canvas.toObject();
    console.log(canvasObj);
}

function toggleDraw() {
    canvas.isDrawingMode = !canvas.isDrawingMode;
    if (!canvas.isDrawingMode) {
        document.getElementById('status').innerHTML = 'Filling';
        document.getElementById('toggleDrawBtn').innerHTML = 'Draw';
        canvas.getObjects().forEach(o => {
            o.hasControls = false;
            o.lockMovementX = true;
            o.lockMovementY = true;
            if (o.isType('path')) {
                //o.perPixelTargetFind = true;
                o.on('selected', function() {
                    var pointer = canvas.getPointer('selected', true);
                    console.log(pointer);
                    if (o.isPartiallyOnScreen()) {
                        extendToBorder(o);
                    }

                    o.set({
                        fill: o.stroke,
                        dirty: true
                    });

                    canvas.discardActiveObject(); // So it won't stay selected, which would disallow re-selection
                    lastAction = {'action': 'fill', 'target': o};
                    actionStack.push(lastAction);
                  });
            }
        });
    } else {
        document.getElementById('status').innerHTML = 'Drawing';
        document.getElementById('toggleDrawBtn').innerHTML = 'Fill';
    }
}

function clearCanvas() {
    canvas.getObjects('path').forEach(o => {
        canvas.remove(o);
    });

    actionStack.forEach(item => {
        item = null;
    });

    lastAction = {};
    canvas.isDrawingMode = true;
    document.getElementById('toggleDrawBtn').innerHTML = 'Fill';
};

function undo() {
    var undoAction = actionStack.pop();
    if (undoAction.action == 'fill') {
        console.log("undoAction was fill with target: " + undoAction.target);
        undoAction.target.set({
            fill: null
        });
        canvas.renderAll();
    } else {
        var lastObj = undoAction.target;
        
        if (!lastObj.isType('image')) {
            canvas.remove(lastObj);
        }
    }
    return;
};

// TODO Expand fill to border when top, left, bottom, right of path are at or beyond the border
function extendToBorder(path) {
    console.log("Path intersects border");
    var boundingRect = path.getBoundingRect(true);
    console.log(boundingRect);
};

function setFull() {
    canvas.freeDrawingBrush.color = 'rgb(191, 96, 0)';
}

function setPartial() {
    canvas.freeDrawingBrush.color = 'rgb(255, 255, 0)';
}

function setErase() {
    canvas.freeDrawingBrush.color = 'rgb(255, 255, 255)';
}

function setBlack() {
    canvas.freeDrawingBrush.color = 'rgb(0, 0, 0)';
}

function save() {
    console.log("Downloading...");
    var a = document.createElement('a');
    var imgUrl = canvas.toDataURL('png');
    a.href = imgUrl;
    a.download = "chart.png";
    document.body.appendChild(a);
    a.click();
}